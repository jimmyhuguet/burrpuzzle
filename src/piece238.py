
from .piece import Piece
import json
import pickle
import numpy as np

class Piece238(Piece):
    def __gui__(self):
        if self.sizes[1] != 2 or self.sizes[2] != 3:
            print(self.sizes)
            raise Exception("wrong method")
        charac = " -+="
        l0 = "".join([charac[self.cubies_at(x,0,2) + self.cubies_at(x,1,2) * 2] for x in range(self.sizes[0])])
        l1 = "".join([charac[self.cubies_at(x,0,1) + self.cubies_at(x,1,1) * 2] for x in range(self.sizes[0])])
        l2 = "".join([charac[self.cubies_at(x,0,0) + self.cubies_at(x,1,0) * 2] for x in range(self.sizes[0])])
        return f"{l0}\n{l1}\n{l2}"

    
    def is_notchable(self):
        try:
            return self._notchable
        except Exception as e:
            self._notchable = True
            for s in self.slices:
                if s not in [1, 2, 4, 8, 5, 10, 16, 32, 20, 40, 21, 42, 3, 12, 15, 48, 60, 63]:
                    self._notchable = False
            return self._notchable

def get_all_piece238_for_6pb():
    savefile_name = "6pb_unique_pieces238.pickle"
    try:
        unique_pieces = pickle.load(open(savefile_name, "rb"))
        print(f"Loaded unique pieces: {len(unique_pieces)}")
    except (OSError, IOError) as e:
        i = 0
        c = 0
        turned = 0
        rotated = 0
        broken = 0
        unique_pieces = []
        for s1 in range(4):
            for s2 in range(4):
                print(f"{s1}, {s2}")
                for s3 in range(64):
                    if s3 == 0 or (s3 % 16 == 0 and (s2 == 0 or (s2 == 1 and s3 == 32) or (s2 == 2 and s3 == 16))):
                        broken += 1024
                        continue
                    for s4 in range(64):
                        if s4 == 0:
                            broken += 16
                            continue
                        for s5 in range(4):
                            for s6 in range(4):
                                p = Piece238([2,3], [63, (s1 << 4) + 15, (s2 << 4) + 15, s3, s4, (s5 << 4) + 15,(s6 << 4) + 15,63])
                                if not p.is_contiguous():
                                    broken += 1
                                    continue
                                t = p.turn_around()
                                if t < p:
                                    turned += 1
                                else:
                                    unique_pieces.append(p)
                                    c+=1
                                i+=1
        print(f"broken pieces eliminated: {broken}")
        print(f"turned pieces eliminated: {turned}")
        print(f"rotated pieces eliminated: {rotated}")
        print(f"unique pieces: {c}")
        pickle.dump(unique_pieces, open(savefile_name, "wb"))
    return unique_pieces

def get_all_notchable_piece238_for_6pb():
    unique_pieces = get_all_piece238_for_6pb()
    notchable_pieces = []
    for piece in unique_pieces:
        if piece.is_notchable():
            notchable_pieces.append(piece)

    print(f"Unique notchable piece: {len(notchable_pieces)}")
    return notchable_pieces


# def make_piece_graph_for_6pb(unique_pieces):
#     try:
#         graph = pickle.load(open("6pb_unique_pieces22_graph.pickle", "rb"))
#     except (OSError, IOError) as e:
#         graph = {}
#         for i, p in enumerate(unique_pieces):
#             # sub are pieces which are identical to piece with one cubies removed
#             # sup are pieces which are identical to piece with one cubies added
#             graph[i] = {"piece": p, "sub": [], "sup":[]}
#         for i1, p1 in enumerate(unique_pieces):
#             for i2, p2 in enumerate(unique_pieces):
#                 if i2 < i1: # avoid doing pair twice
#                     continue
#                 if abs(p1.cubies_count() - p2.cubies_count()) != 1:
#                     continue
#                 print(i1, i2)
#                 r1 = p1.rotate()
#                 r2 = r1.rotate()
#                 r3 = r2.rotate()
#                 t = p1.turn_around()
#                 t1 = t.rotate()
#                 t2 = t1.rotate()
#                 t3 = t2.rotate()
#                 for pi in [p1,r1,r2,r3,t,t1,t2,t3]:
#                     cubies_added, cubies_removed = Piece.count_changes(pi, p2)
#                     if cubies_added == 1 and cubies_removed == 0:
#                         graph[i1]["sup"].append(i2)
#                         graph[i2]["sub"].append(i1)
#                         break
#                     if cubies_added == 0 and cubies_removed == 1:
#                         graph[i2]["sup"].append(i1)
#                         graph[i1]["sub"].append(i2)
#                         break
#         pickle.dump(graph, open("6pb_unique_pieces22_graph.pickle", "wb"))
#     return graph

# def explore():
#     unique_pieces = get_all_piece22_for_6pb()
#     graph = make_piece_graph_for_6pb(unique_pieces)

#     l1 = ""
#     l2 = ""
#     for i, p in enumerate(unique_pieces):
#         g = p.__gui__()
#         spl = g.split("\n")
#         l1 += "  " + spl[0]
#         l2 += "  " + spl[1]
#         if (i+1) % 20 == 0:
#             print(l1)
#             print(l2)
#             print("")
#             l1 = ""
#             l2 = ""

#     for idx, obj in graph.items():
#         piece = obj["piece"]
#         sub = obj["sub"]
#         sup = obj["sup"]
#         print(f"{idx}, sub: {sub}, sup: {sup}")