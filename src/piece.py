import numpy as np
# import pymesh # https://pymesh.readthedocs.io/en/latest/index.html
import traceback

class Piece():

    def __init__(self, sizes, slices, ratio=1):
        self.sizes = [len(slices), *sizes]
        self.slices = slices
        self.x_len = len(self.slices)
        self.cubies = self.cubies()
        self.ratio = ratio
        self.holes = 0
        for slice in self.cubies:
            for cubie in slice:
                if not cubie:
                    self.holes += 1

    def __str__(self):
        sizes = ".".join([f"{i}" for i in self.sizes])
        slices = ".".join([f"{i}" for i in self.slices])
        return f"{sizes}_{slices}"


    def __lt__(self, other):
        if self.sizes != other.sizes:
            raise ValueError
        if len(self.slices) < len(other.slices):
            return True
        for sidx, _ in enumerate(self.slices):
            if self.slices[sidx] < other.slices[sidx]:
                return True
            elif self.slices[sidx] > other.slices[sidx]:
                return False
        return False

    def __eq__(self, other) -> bool:
        if self.sizes != other.sizes:
            raise ValueError
        for sidx, _ in enumerate(self.slices):
            if self.slices[sidx] != other.slices[sidx]:
                return False
        return True
        
    def turn_around(self):
        def turn(s):
            ns = 0
            for y,z in np.ndindex(self.sizes[1], self.sizes[2]):
                ns += (s >> (z * self.sizes[1] + y)) % 2 << (z * self.sizes[1] + (self.sizes[1] - y - 1))
            return ns

        slices = [turn(s) for s in self.slices[::-1]]
        return Piece(self.sizes[1:], slices)
    
    def rotate(self):
        def rots(s):
            ns = 0
            for y,z in np.ndindex(self.sizes[1], self.sizes[2]):
                ns += (s >> (z * self.sizes[1] + y)) % 2 << ((self.sizes[1] - y - 1) * self.sizes[2] + z)
            return ns

        slices = [rots(s) for s in self.slices]
        return Piece(self.sizes[1:][::-1], slices)

    @staticmethod 
    def count_changes(p1, p2):
        if p1.sizes[0] != p2.sizes[0] or p1.sizes[1] != p2.sizes[1] or p1.sizes[2] != p2.sizes[2]:
            raise ValueError
        cubies_removed = 0
        cubies_added = 0
        for x,y,z in np.ndindex((p1.sizes[0],p1.sizes[1], p1.sizes[2])):
            c1 = p1.cubies_at(x,y,z)
            c2 = p2.cubies_at(x,y,z)
            if c1 and not c2:
                cubies_removed += 1
            elif not c1 and c2:
                cubies_added += 1
        return cubies_added, cubies_removed

    def is_contiguous(self):
        notit = []
        it = []
        for x,y,z in np.ndindex((self.sizes[0],self.sizes[1], self.sizes[2])):
            if self.cubies_at(x,y,z):
                cur = (x, y, z)
                notit.append(cur)
        for _ in range(max(self.sizes) * 2):
            if len(notit) == 0:
                return True
            next_notit = []
            for cur in notit:
                if len(it) == 0:
                    it.append(cur)
                    continue
                x = cur[0]
                y = cur[1]
                z = cur[2]
                around = [(x,y,z-1), (x,y,z+1), (x,y-1,z), (x,y+1,z), (x-1,y,z), (x+1,y,z)]
                for pos in around:
                    if pos in it:
                        it.append(cur)
                        break
                else:
                    next_notit.append(cur)
            notit = next_notit
        return len(notit) == 0


    def cubies_at(self, x, y, z):
        return self.cubies[x][y][z]

    def cubies_count(self):
        try:
            return self._cubies_count
        except Exception as e:
            self._cubies_count = sum([self.cubies_at(x,y,z) for x,y,z in np.ndindex((self.sizes[0],self.sizes[1], self.sizes[2]))])
            return self._cubies_count


    def cubies(self):
        return [[[(s >> (z*self.sizes[1]+y)) % 2 == 1 for z in range(self.sizes[2])] for y in range(self.sizes[1])] for s in self.slices]

    def mesh_sub(self):
        """ Generate a 3D mesh of the piece using constructive solid geometry (CSG):
                * First make a `box`
                * add a `square frustum` at the bottom to create the shape of the piece with a chanfrein
                * for each missing cubie:
                    * create a `box`
                    * add a `square frustum` at the bottom if cubie is located at the bottom of the piece
                    * remove the cubie from the main shape
        """
        chanfrein_size = 0.2
        # array of 8 points (each corner of the box), each point is an array of 3 float: [[x,y,z], ...]
        piece_vertices = np.array([[
            float((i >> 0) % 2) * len(self.slices),
            float((i >> 1) % 2) * self.sizes[1],
            float((i >> 2) % 2) * self.sizes[2] + (chanfrein_size if float((i >> 2) % 2) == 0 else 0),
        ] for i in range(8)])

        # array of triangles (2 per face), each value is the index of the points in piece_vertices
        # faces direction is important to keep a constant piecewise winding number (constant PWN)
        # constant PWN is required to perform boolean operation
        faces = np.array([[0,2,3],[0,3,1],[0,5,4],[0,1,5],[0,6,2],[0,4,6],[4,7,6],[4,5,7],[1,7,5],[1,3,7],[3,2,7],[7,2,6]])

        # make the mesh for the box
        pmesh = pymesh.form_mesh(piece_vertices, faces)

        # helper method to define the chanfrein ratio
        def enlarge(x):
            return (x-0.5) * 1.05 + 0.5

        # array of 8 points (each corner of the frustum)
        chanfrein_vertices = np.array([[
            float((i >> 0) % 2) * len(self.slices) + ((chanfrein_size if (float((i >> 0) % 2) == 0) else -chanfrein_size) if float((i >> 2) % 2) == 0 else 0),
            float((i >> 1) % 2) * self.sizes[1] + ((chanfrein_size if (float((i >> 1) % 2) == 0) else -chanfrein_size) if float((i >> 2) % 2) == 0 else 0),
            float((i >> 2) % 2) * chanfrein_size,
        ] for i in range(8)])

        # create the mesh of the frustum and add it to the main shape
        chanfrein = pymesh.form_mesh(chanfrein_vertices, faces)
        pmesh = pymesh.boolean(pmesh, chanfrein, "union", engine="igl")

        # hole mesh will be the shape to remove from the main shape
        hole_mesh = None
        # for each cubies of our piece
        for x,y,z in np.ndindex((self.sizes[0],self.sizes[1], self.sizes[2])):
            if not self.cubies_at(x,y,z): # if the cubie is a hole (no cubie)
                position = [x, y, z]
                # box vertices and mesh
                cube_vertices = np.array([[enlarge(float((i >> j) % 2)) + position[j] + (chanfrein_size if j == 2 and z == 0 and (float((i >> j) % 2) == 0) else 0) for j in range(3)] for i in range(8)])
                cube_mesh = pymesh.form_mesh(cube_vertices, faces)

                # eventually add a chanfrein
                if z == 0:
                    chanfrein_vertices = np.array([[
                        enlarge(float((i >> 0) % 2)) + position[0]  + ((-chanfrein_size if (float((i >> 0) % 2) == 0) else chanfrein_size) if float((i >> 2) % 2) == 0 else 0),
                        enlarge(float((i >> 1) % 2)) + position[1]  + ((-chanfrein_size if (float((i >> 1) % 2) == 0) else chanfrein_size) if float((i >> 2) % 2) == 0 else 0),
                        enlarge(float((i >> 2) % 2)) * chanfrein_size,
                    ] for i in range(8)])
                    chanfrein = pymesh.form_mesh(chanfrein_vertices, faces)
                    cube_mesh = pymesh.boolean(cube_mesh, chanfrein, "union", engine="igl")

                print(f"cube diff {position}")
                pmesh = pymesh.boolean(pmesh, cube_mesh, "difference", engine="igl")
                #pmesh, info = pymesh.collapse_short_edges(pmesh, 0.1)
                # print(f"info {info}")
                # if hole_mesh == None:
                #     hole_mesh = cube_mesh
                # else:
                #     hole_mesh = pymesh.boolean(hole_mesh, cube_mesh, "union", engine="igl")

        # remove the `holes` from the main shape to form the final piece mesh
        return pmesh #pymesh.boolean(pmesh, hole_mesh, "difference", engine="igl")

    def save(self, marker=""):
        try:
            name = str(self)
            path = f"stls/{marker}{name}.stl"
            print(f"generating mesh for: {name}")
            pmesh = self.mesh_sub()
            pymesh.save_mesh(f"stls/{marker}{name}.stl", pmesh)
            print(f"saved: {path}")
        except Exception as e:
            print(e)
            traceback.print_exc()
