from .piece import Piece
import json
import pickle
import numpy as np


def get_all_notchable_piece_for_6pb_66():
    try:
        unique_pieces = pickle.load(open("6pb66_unique_pieces.pickle", "rb"))
    except (OSError, IOError) as e:
        i = 0
        c = 0
        turned = 0
        rotated = 0
        broken = 0
        unique_pieces = []

        full = 2**36-1
        hfull = 2**18 - 1
        hrow = 2**6 - 1
        row = 2**12 - 1
        square = (2**2 - 1) + ((2**2 - 1) << 6)
        pidx = 0
        colors = ["0;32","0;31","0;33","0;37","0;34","0;35"]
        brokenmap = [
            [False,False,False,False,False,False] # 0
            ,[False,False,False,False,True,False] # 1
            ,[False,False,False,True,True,True] # 2
            ,[False,False,True,False,False,False] # 3
            ,[False,True,True,False,False,True] # 4
            ,[False,False,True,False,True,False] # 5
        ]
        for n1 in range(6):
            for n2h, n2v in np.ndindex(6,6):
                if n2h == 4 and n1 > 1:
                    continue
                for n3h, n3v in np.ndindex(6,6):
                    if brokenmap[n2h][n3h] or brokenmap[n2v][n3v]:
                        continue
                    for n4h, n4v in np.ndindex(n2h+1,6):
                        if brokenmap[n3h][n4h] or brokenmap[n3v][n4v]:
                            continue
                        for n5 in range(n1+1):
                            def get_side(n):
                                h11 = n % 2 == 1
                                h12 = n == 5
                                h21 = n > 1
                                h22 = n > 3
                                s1 = hfull + (hrow << 18 if not h12 else 0) + (row << 24 if not h11 else 0)
                                s2 = hfull + (hrow << 18 if not h22 else 0) + (row << 24 if not h21 else 0)
                                return s1, s2
                            def get_internal(nh, nv):
                                h31 = nh % 2 == 1 or nh == 2
                                h32 = nh == 2 or nh == 4
                                h33 = nh > 2
                                v31 = nv % 2 == 1 or nv == 2
                                v32 = nv == 2 or nv == 4
                                v33 = nv > 2
                                s = (((square << 0  if not h31 else 0) + (square << 2 if not h32 else 0) + (square << 4 if not h33 else 0)) if not v31 else 0) +\
                                    (((square << 12 if not h31 else 0) + (square << 14 if not h32 else 0) + (square << 16 if not h33 else 0)) if not v32 else 0) +\
                                    (((square << 24 if not h31 else 0) + (square << 26 if not h32 else 0) + (square << 28 if not h33 else 0)) if not v33 else 0)
                                return s

                            s1, s2 = get_side(n1)
                            s3 = get_internal(n2h,n2v)
                            s4 = get_internal(n3h,n3v)
                            s5 = get_internal(n4h,n4v)
                            s7, s6 = get_side(n5)

                            p = Piece([6,6], [full, full, full, s1, s2,s2, s3,s3, s4,s4, s5,s5, s6,s6, s7, full, full, full])
                            pidx += 1
                            if not p.is_contiguous():
                                broken += 1
                                print(f"\033[{colors[1]}m broken: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5}\033[0m")
                                continue
                            t = p.turn_around()
                            vp = [t]
                            if n1 == 0 and n5 == 0:
                                r1 = p.rotate()
                                r2 = r1.rotate()
                                r3 = r2.rotate()
                                t1 = t.rotate()
                                t2 = t1.rotate()
                                t3 = t2.rotate()
                                for pi in [r1,r2,r3,t1,t2,t3]:
                                    vp.append(pi)
                            for pi in vp:
                                if pi < p:
                                    if pi == t:
                                        turned += 1
                                        print(f"\033[{colors[3]}m turned: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5} \033[0m")
                                    else:
                                        rotated += 1
                                        print(f"\033[{colors[2]}m rotated: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5} \033[0m")
                                    break
                            else:
                                # print(f"{c}({i}): {p.slices}")
                                unique_pieces.append(p)
                                # p.save(f"test__{pidx}__")
                                print(f"\033[{colors[0]}m add: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5} \033[0m")
                                c+=1
                            i+=1
        print(f"broken pieces eliminated: {broken}")
        print(f"turned pieces eliminated: {turned}")
        print(f"rotated pieces eliminated: {rotated}")
        print(f"unique pieces: {c}")
        pickle.dump(unique_pieces, open("6pb66_unique_pieces.pickle", "wb"))
    return unique_pieces

def get_all_notchable_piece_for_6pb_44():
    try:
        unique_pieces = pickle.load(open("6pb44_unique_pieces.pickle", "rb"))
        print(f"unique piece: {len(unique_pieces)}")
    except (OSError, IOError) as e:
        i = 0
        c = 0
        turned = 0
        rotated = 0
        broken = 0
        unique_pieces = []

        full = 2**16-1
        hfull = 2**8 - 1
        row = 2**4 - 1
        square = (2**2 - 1) + ((2**2 - 1) << 4)
        pidx = 0
        colors = ["0;32","0;31","0;33","0;37","0;34","0;35"]
        def get_side(n):
            h1 = n > 0
            h2 = n == 2
            s = hfull + (row << 8 if not h2 else 0) + (row << 12 if not h1 else 0)
            return s
        def get_internal(nh, nv):
            h1 = nh in [1,2,3,5,6,8]
            h2 = nh in [2,3,6,9]
            h3 = nh in [3,7,8,9]
            h4 = nh in [4,5,6,7,8,9]
            v1 = nv in [1,2,3,5,6,8]
            v2 = nv in [2,3,6,9]
            v3 = nv in [3,7,8,9]
            v4 = nv in [4,5,6,7,8,9]
            hs = [h1,h2,h3,h4]
            vs = [v1,v2,v3,v4]
            s = sum([sum([1 << (j + 4*i) if hs[j] else 0 for j in range(4)]) if vs[i] else 0 for i in range(4)])
            return s
        assert get_internal(0,0) == full
        assert get_internal(1,0) == 2**12-1
        assert get_internal(2,0) == hfull
        assert get_internal(3,0) == row
        assert get_internal(0,1) == (2**3 - 1) << 1 + (2**3 - 1) << 5 + (2**3 - 1) << 9 + (2**3 - 1) << 13
        assert get_internal(0,2) == (2**2 - 1) << 2 + (2**2 - 1) << 6 + (2**2 - 1) << 10 + (2**2 - 1) << 14
        brokenmap = [
            [False,False,False,False,False,False] # 0
            ,[False,False,False,False,True,False] # 1
            ,[False,False,False,True,True,True] # 2
            ,[False,False,True,False,False,False] # 3
            ,[False,True,True,False,False,True] # 4
            ,[False,False,True,False,True,False] # 5
        ]
        for n1, n2 in np.ndindex(3,3):
            for n3h, n3v in np.ndindex(10,10):
                for n4h, n4v in np.ndindex(10,10):
                    for n5h, n5v in np.ndindex(10,10):
                        for n6h, n6v in np.ndindex(10,10):
                            for n7, n8 in np.ndindex(3,3):
                                for n5 in range(n1+1):
                                    
                                    

                                    s1, s2 = get_side(n1)
                                    s3 = get_internal(n2h,n2v)
                                    s4 = get_internal(n3h,n3v)
                                    s5 = get_internal(n4h,n4v)
                                    s7, s6 = get_side(n5)

                                    p = Piece([4,4], [full, full, s1, s2, s3, s4, s5, s6, s7, s8, full, full])
                                    pidx += 1
                                    if not p.is_contiguous():
                                        broken += 1
                                        print(f"\033[{colors[1]}m broken: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5}\033[0m")
                                        continue
                                    t = p.turn_around()
                                    vp = [t]
                                    if n1 == 0 and n5 == 0:
                                        r1 = p.rotate()
                                        r2 = r1.rotate()
                                        r3 = r2.rotate()
                                        t1 = t.rotate()
                                        t2 = t1.rotate()
                                        t3 = t2.rotate()
                                        for pi in [r1,r2,r3,t1,t2,t3]:
                                            vp.append(pi)
                                    for pi in vp:
                                        if pi < p:
                                            if pi == t:
                                                turned += 1
                                                print(f"\033[{colors[3]}m turned: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5} \033[0m")
                                            else:
                                                rotated += 1
                                                print(f"\033[{colors[2]}m rotated: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5} \033[0m")
                                            break
                                    else:
                                        # print(f"{c}({i}): {p.slices}")
                                        unique_pieces.append(p)
                                        # p.save(f"test__{pidx}__")
                                        print(f"\033[{colors[0]}m add: {n1},{n2h},{n2v},{n3h},{n3v},{n4h},{n4v},{n5} \033[0m")
                                        c+=1
                                    i+=1
        print(f"broken pieces eliminated: {broken}")
        print(f"turned pieces eliminated: {turned}")
        print(f"rotated pieces eliminated: {rotated}")
        print(f"unique pieces: {c}")
        pickle.dump(unique_pieces, open("6pb44_unique_pieces.pickle", "wb"))
    return unique_pieces

def all_pieces_combination(graph):
    def starting_from(i1,i2,i3,i4,i5,i6):
        count = 0
        if i5 < i6 or i4 < i5 or i3 < i4 or i2 < i3 or i1 < i2:
            return 0
        count += 1
        for s6 in graph[i6]["sup"]:
            count += starting_from(i1,i2,i3,i4,i5,s6)
        for s5 in graph[i5]["sup"]:
            count += starting_from(i1,i2,i3,i4,s5,i6)
        for s4 in graph[i4]["sup"]:
            count += starting_from(i1,i2,i3,s4,i5,i6)
        print(f"{i1},{i2},{i3},{i4},{i5},{i6}")
        for s3 in graph[i3]["sup"]:
            count += starting_from(i1,i2,s3,i4,i5,i6)
        for s2 in graph[i2]["sup"]:
            count += starting_from(i1,s2,i3,i4,i5,i6)
        for s1 in graph[i1]["sup"]:
            count += starting_from(s1,i2,i3,i4,i5,i6)
        return 1
    return starting_from(0,0,0,0,0,0)

def display_graph_as_tree(unique_pieces, graph):
    lines = ["", ""]
    drawn = set({})
    def rec(pi, recur_idx):
        p = unique_pieces[pi]
        g = p.__gui22__()
        spl = g.split("\n")
        lines[0] += " " + spl[0] + f" ({pi})"
        lines[1] += " " + spl[1] + "     "
        drawn.add(pi)
        for idx in graph[pi]["sup"]:
            if recur_idx > 3:
                return
            lines[1] += "|->   "
            while len(lines[0]) < len(lines[1]):
                lines[0] += " "
            if idx in drawn:
                lines[1] += f" ({pi}) "
                while len(lines[0]) < len(lines[1]):
                    lines[0] += " "
                continue
            rec(idx, recur_idx+1)
            print("            |     " * (recur_idx))
            print(lines[0])
            print(lines[1])
            lines[0] = "            |     " * (recur_idx-1) + "            |     "
            lines[1] = "            |     " * (recur_idx-1) + "            "
            
    rec(0,1)

        

def explore():
    unique_pieces = get_all_piece_for_6pb()
    graph = make_piece_graph_for_6pb(unique_pieces)
    # display_graph_as_tree(unique_pieces, graph)
    l1 = ""
    l2 = ""
    for i, p in enumerate(unique_pieces):
        g = p.__gui22__()
        spl = g.split("\n")
        l1 += "  " + spl[0]
        l2 += "  " + spl[1]
        if (i+1) % 20 == 0:
            print(l1)
            print(l2)
            print("")
            l1 = ""
            l2 = ""

    for idx, obj in graph.items():
        piece = obj["piece"]
        sub = obj["sub"]
        sup = obj["sup"]
        print(f"{idx}, sub: {sub}, sup: {sup}")
    return
    all_pieces_combination(graph)

    # final assembled puzzle takes 104 cubies, 72 are visible
    # the inner/hidden space is 32 cubies
    # half the piece's inner cubies must be holes at least for assemblies to fit the available space
    required_holes = 36

    all_pieces = get_all_piece_for_6pb()
    potential_combination = 0
    pair_combination = {}
    for i1, p1 in enumerate(all_pieces):
        for i2, p2 in enumerate(all_pieces):
            if i2 < i1: # avoid counting pair twice
                continue
            if p1.holes + p2.holes not in pair_combination:
                pair_combination[p1.holes + p2.holes] = 0
            pair_combination[p1.holes + p2.holes] += 1
    print(pair_combination)

    sixte_combination = {}
    for h1, p1 in pair_combination.items():
        for h2, p2 in pair_combination.items():
            for h3, p3 in pair_combination.items():
                total_holes = h1+h2+h3
                if total_holes not in sixte_combination:
                    sixte_combination[total_holes] = 0
                sixte_combination[total_holes] += p1*p2*p3
                if total_holes >= required_holes:
                    potential_combination += p1*p2*p3
    print(potential_combination / 36)
    print(sixte_combination)