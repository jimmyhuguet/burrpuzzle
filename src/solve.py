from .piece import Piece
import itertools
import enum
import json
import copy
import numpy as np
import hashlib

def Dd(x,y,z): return x,y+2,z+1
def Di(x,y,z): return 5-x,3-y,z+1
def Ud(x,y,z): return x,3-y,4-z
def Ui(x,y,z): return 5-x,y+2,4-z

def Ld(x,y,z): return z+1,x,y+2
def Li(x,y,z): return z+1,5-x,3-y
def Rd(x,y,z): return 4-z,x,3-y
def Ri(x,y,z): return 4-z,5-x,y+2

def Fd(x,y,z): return y+2,z+1,x
def Fi(x,y,z): return 3-y,z+1,5-x
def Bd(x,y,z): return 3-y,4-z,x
def Bi(x,y,z): return y+2,4-z,5-x

mappings = {"D^":Dd,"Dv":Di,"U^":Ud,"Uv":Ui,"L^":Ld,"Lv":Li,"R^":Rd,"Rv":Ri,"F^":Fd,"Fv":Fi,"B^":Bd,"Bv":Bi}

class Assembly():
    def __init__(self, x_len, y_len, z_len):
        self.x_len = x_len
        self.y_len = y_len
        self.z_len = z_len
        self.cubies = [[[None  for z in range(z_len)]  for y in range(y_len)] for x in range(x_len)]
        self.piece_count = 0

    def copy(self):
        c = Assembly(0,0,0)
        c.cubies = copy.deepcopy(self.cubies)
        c.x_len = self.x_len
        c.y_len = self.y_len
        c.z_len = self.z_len
        c.piece_count = self.piece_count
        return c

    def add_piece(self, p, position, i):
        map = mappings[position]
        for x in range(6):
            for y in range(2):
                for z in range(2):
                    if p.cubies_at(x,y,z):
                        ax,ay,az = map(x,y,z)
                        if self.cubies[ax][ay][az] != None:
                            return False
                        self.cubies[ax][ay][az] = i
        self.piece_count += 1
        return True

    def color_str(self, x,y,z):
        val = self.cubies[x][y][z]
        colors = ["0;32","0;31","0;33","0;37","0;34","0;35"]
        return f"\033[{colors[val]}m{val}\033[0m"

    def _str_array(self):
        line = "".join(['-' for i in range(9*6+1)])
        return line + "\n" + "\n".join([
            "| " + " | ".join([
                "".join([
                    ' ' if self.cubies[x][y][z] == None else self.color_str(x,y,z) for x in range(self.x_len)
                ]) for y in range(self.y_len)
            ]) + " |"  for z in range(self.z_len)
        ]) + "\n" + line

    def __str__(self):
        return self._str_array()
        # return str(self.unique_hash()) + "\n" + self._str_array()

    def unique_hash(self):
        hash_object = hashlib.md5(self._str_array().encode())
        return hash_object.hexdigest()

    def move(self, pi, move):
        if not isinstance(pi, list):
            pi = [pi]
        ncubies = [[[None  for z in range(self.z_len)]  for y in range(self.y_len)] for x in range(self.x_len)]
        shifted = False
        shiftx = 0
        shifty = 0
        shiftz = 0
        for x,y,z in np.ndindex((self.x_len,self.y_len, self.z_len)):
            if self.cubies[x][y][z] in pi:
                if move == 'l':
                    if x == 0 and not shifted:
                        shifted = True
                        shiftx = 1
                        self.x_len += 1
                        ncubies.insert(0, [[None  for zb in range(self.z_len)]  for yb in range(self.y_len)])
                    ncubies[x-1+shiftx][y][z] = self.cubies[x][y][z]
                elif move == 'r':
                    if x == self.x_len-1 and not shifted:
                        shifted = True
                        self.x_len += 1
                        ncubies.append([[None  for zb in range(self.z_len)]  for yb in range(self.y_len)])
                    ncubies[x+1][y][z] = self.cubies[x][y][z]
                elif move == 'f':
                    if y == 0 and not shifted:
                        shifted = True
                        shifty = 1
                        self.y_len += 1
                        for xb in range(self.x_len):
                            ncubies[xb].insert(0, [None  for zb in range(self.z_len)])
                    ncubies[x][y-1+shifty][z] = self.cubies[x][y][z]
                elif move == 'b':
                    if y == self.y_len-1 and not shifted:
                        shifted = True
                        self.y_len += 1
                        for xb in range(self.x_len):
                            ncubies[xb].append([None  for zb in range(self.z_len)])
                    ncubies[x][y+1][z] = self.cubies[x][y][z]
                elif move == 'd':
                    if z == 0 and not shifted:
                        shifted = True
                        shiftz = 1
                        self.z_len += 1
                        for xb,yb in np.ndindex((self.x_len,self.y_len)):
                            ncubies[xb][yb].insert(0, None)
                    ncubies[x][y][z-1+shiftz] = self.cubies[x][y][z]
                elif move == 'u':
                    if z == self.z_len-1 and not shifted:
                        shifted = True
                        self.z_len += 1
                        for xb,yb in np.ndindex((self.x_len,self.y_len)):
                            ncubies[xb][yb].append(None)
                    ncubies[x][y][z+1] = self.cubies[x][y][z]
            else:
                if ncubies[x+shiftx][y+shifty][z+shiftz] not in pi:
                    ncubies[x+shiftx][y+shifty][z+shiftz] = self.cubies[x][y][z]
        self.cubies = ncubies

    def remove_piece(self, pi):
        for x,y,z in np.ndindex((self.x_len,self.y_len, self.z_len)):
            if self.cubies[x][y][z] == pi:
                self.cubies[x][y][z] = None


class Puzzle():
    def __init__(self, pieces):
        self.pieces = pieces

    def solve_6pb(self):
        if len(self.pieces) != 6:
            raise Exception("cannot solve a 6 piece burr that doesn't have 6 piece")
  
        all_assembly = self.find_all_6pb_assembly()
        solvable = {}
        for idx, a in enumerate(all_assembly):
            # print(a)
            all_sequences = self.find_disassembly(a)
            if len(all_sequences) != 0:
                solvable[idx] = all_sequences
                # print(str(a))
                # print(f"sequences: {all_sequences}")
        # print(f"{len(all_assembly)} assemblies found, {solvable} can be disassembled")
        return all_assembly, solvable

    def get_moves_restriction(self, assembly):
        moves_restriction = {i : {m: None for m in "udrlbf"} for i in range(6)}
        for x,y,z in np.ndindex((assembly.x_len,assembly.y_len, assembly.z_len)):
            pi = assembly.cubies[x][y][z]
            if pi != None:
                moves = moves_restriction[pi]
                cases = {
                    'u': {'vector': [0,0,1], 'test': z != assembly.z_len-1},
                    'd': {'vector': [0,0,-1], 'test': z != 0},
                    'r': {'vector': [1,0,0], 'test': x != assembly.x_len-1},
                    'l': {'vector': [-1,0,0], 'test': x != 0},
                    'b': {'vector': [0,1,0], 'test': y != assembly.y_len-1},
                    'f': {'vector': [0,-1,0], 'test': y != 0},
                }
                for mi, move in moves.items():
                    case = cases[mi]
                    vector = case['vector']
                    if case['test']:
                        cubie = assembly.cubies[x+vector[0]][y+vector[1]][z+vector[2]]
                        if cubie not in [None,pi]:
                            if moves[mi] == None:
                                moves[mi] = set({})
                            moves[mi].add(cubie)

        # Find pieces that have no restriction (can be removed)
        moves_restriction['*'] = set({})
        for pi, move_restriction in moves_restriction.items():
            if pi == '*':
                continue
            for move, restrictions in move_restriction.items():
                if restrictions != None:
                    break
            else:
                moves_restriction['*'].add(pi)

        # Find pair of pieces that can be moved together
        moves = ['u', 'd', 'l', 'r', 'f', 'b']
        for pi1 in range(6):
            for pi2 in range(6):
                if pi2 <= pi1:
                    continue
                for m in moves:
                    m1 = moves_restriction[pi1][m]
                    m2 = moves_restriction[pi2][m]
                    if m1 != None and m2 != None and len(m1) == 1 and len(m2) == 1 and pi1 in m2 and pi2 in m1:
                        if (pi1,pi2) not in moves_restriction:
                            moves_restriction[(pi1,pi2)] = set({})
                        
                        moves_restriction[(pi1,pi2)].add(m)
                        # print(moves_restriction)
        
        return moves_restriction

    def all_valid_piece_orientations_for_6pb(self, piece):
        ori = [piece]
        if not piece == piece.turn_around():
            ori.append(piece.turn_around())
        r1 = piece.rotate()
        r2 = r1.rotate()
        r3 = r2.rotate()
        for ri in [r1,r2,r3]:
            if ri.slices[1] % 4 == 3 and ri.slices[4] % 4 == 3:
                for p in ori:
                    if p == ri:
                        break
                else:
                    ori.append(ri)
                rit = ri.turn_around()
                for p in ori:
                    if p == rit:
                        break
                else:
                    ori.append(rit)
        return ori


    def find_all_6pb_assembly(self):
        # identify identical pieces and avoid duplicate assembly (swapping identical result in identical assembly)
        apo = [] # available pieces orientation
        added = set({})
        for pi1, p1 in enumerate(self.pieces):
            if pi1 in added:
                continue
            pi1_group = [{"pi": pi1, "orientation": self.all_valid_piece_orientations_for_6pb(p1)}]
            added.add(pi1)
            for pi2, p2 in enumerate(self.pieces):
                if pi2 <= pi1:
                    continue
                if p1 == p2:
                    pi1_group.append({"pi": pi2, "orientation": self.all_valid_piece_orientations_for_6pb(p2)})
                    added.add(pi2)
            apo.append(pi1_group)

        full_assembly = []
        def rec(tree, pos_order, apo):
            for tidx, subtree in tree.items():
                if tidx == "assembly":
                    continue
                if len(pos_order) == 0:
                    full_assembly.append(subtree["assembly"])
                ti = int(tidx[0])
                napo = copy.deepcopy(apo)
                for psi, ps in enumerate(apo):
                    if len(ps) == 0:
                        continue
                    i = 0
                    if ps[0]["pi"] == ti:
                        del napo[psi][0]
                        if len(napo[psi]) == 0:
                            continue
                        i += 1
                    for orii, ori in enumerate(ps[i]["orientation"]):
                        assembly_copy = subtree["assembly"].copy()
                        if assembly_copy.add_piece(ori, f'{pos_order[0]}^', ps[i]["pi"]):
                            subtree[f'{ps[i]["pi"]}{orii}'] = {"assembly": assembly_copy}
                rec(subtree, pos_order[1:], napo)

        pos_order = "DLFURB"
        assembly = Assembly(6,6,6)
        assembly.add_piece(self.pieces[0], f"{pos_order[0]}^", 0)
        assembly_tree = {"00":{"assembly": assembly}}
        del apo[0][0]
        rec(assembly_tree, pos_order[1:], apo)

        return full_assembly

    def find_disassembly(self, assembly):
        move_inverse = {'u':'d','d':'u','l':'r','r':'l','f':'b','b':'f'}
        seen_assembly = set({})
        call_queue = []
        all_sequences = []

        def sub(assembly, sequence):
            if assembly.x_len > 12 or assembly.y_len > 12 or assembly.z_len > 12:
                return [] # for now consider this a bug case

            # don't go through a configuration twice
            seen_assembly.add(assembly.unique_hash()) 

            moves_restriction = self.get_moves_restriction(assembly)
            # print(moves_restriction)

            # Remove a piece if we can
            for pi in moves_restriction['*']:
                ca = assembly.copy()
                ca.remove_piece(pi)
                nseq = copy.copy(sequence)
                nseq.append(f"{pi}*")
                all_sequences.append(nseq)

            for pi, move_restriction in moves_restriction.items():
                if pi == '*':
                    continue # Ignore piece removal now (it's done already)

                # If we can move multiple pieces together let's try doing so
                if isinstance(pi, tuple):
                    idx = "-".join(map(str, pi))
                    for move in move_restriction:
                        if len(sequence) > 0 and sequence[-1] == f"{idx}{move_inverse[move]}":
                            continue # don't do last move in reverse
                        ca = assembly.copy()
                        ca.move(list(pi), move)
                        if ca.unique_hash() in seen_assembly:
                            continue # Ignore configuration we have been to seen
                        nseq = copy.copy(sequence)
                        nseq.append(f"{idx}{move}")
                        call_queue.append((ca, nseq))
                    continue

                # Let's also try to move pieces on their own
                for move, restrictions in move_restriction.items():
                    if len(sequence) > 0 and sequence[-1] == f"{pi}{move_inverse[move]}":
                        continue # don't do last move in reverse
                    if restrictions == None:
                        ca = assembly.copy()
                        ca.move(pi, move)
                        if ca.unique_hash() in seen_assembly:
                            continue
                        nseq = copy.copy(sequence)
                        nseq.append(f"{pi}{move}")
                            # TODO: Breadth-first search
                        call_queue.append((ca, nseq))

        sub(assembly, [])
        while len(call_queue) > 0:
            next_call = call_queue.pop(0)
            sub(next_call[0], next_call[1])
            if len(all_sequences) > 0:
                break

        return all_sequences