from .piece import Piece

# In the following puzzles:
# * unique: means there is only one solution
# * non-unique: means there is multiple ways to assemble (if specified, level is the highest amongst all assemblies)
# * level-x: means x movement are required from the assembled position to remove the first piece
# * holes: the number of empty spaces in the assembled puzzle

# the followings are taken from http://billcutlerpuzzles.com/docs/CA6PB/examples.html

def bill_baffling_burr():
    """ Computer-assistedly discovered by Bill Cutler on March 3, 1984 - unique level-5 """
    return [
        Piece([2,2], [15,3,2,2,3,15]),
        Piece([2,2], [15,15,3,3,15,15]),
        Piece([2,2], [15,3,3,2,7,15]),
        Piece([2,2], [15,3,2,10,3,15]),
        Piece([2,2], [15,3,10,2,3,15]),
        Piece([2,2], [15,11,2,6,7,15]),
    ]

def piston_puzzle():
    """ Discovered by Peter Marineau in 1986 - unique level-9 - the first level 9 (previous highest was 7)"""
    return [
        Piece([2,2], [15,3,1,1,7,15]),
        Piece([2,2], [15,3,1,1,7,15]),
        Piece([2,2], [15,3,3,1,3,15]),
        Piece([2,2], [15,3,1,3,3,15]),
        Piece([2,2], [15,3,14,14,3,15]),
        Piece([2,2], [15,3,11,3,15,15]),
    ]


def L5_notchable():
    """ Computer-assistedly discovered by Bill Cutler in 1987 - unique level-5
        highest level for unique solutions in notchable 6-piece burrs is 5. This is one of 139 such designs """
    return [
        Piece([2,2], [15,15,3,10,15,15]),
        Piece([2,2], [15,3,3,3,3,15]),
        Piece([2,2], [15,3,2,10,3,15]),
        Piece([2,2], [15,3,10,10,3,15]),
        Piece([2,2], [15,3,10,2,3,15]),
        Piece([2,2], [15,3,10,2,3,15]),
    ]

def Love_dozen():
    """ Discovered by Bruce Love in 1987 - non-unique level-12
        highest level possible for a 6-piece burr, also the only level-12 """
    return [
        Piece([2,2], [15,3,10,2,3,15]),
        Piece([2,2], [15,3,3,10,3,15]),
        Piece([2,2], [15,3,11,3,15,15]),
        Piece([2,2], [15,3,2,10,3,15]),
        Piece([2,2], [15,11,2,2,3,15]),
        Piece([2,2], [15,3,2,3,3,15]),
    ]

def CC3H():
    """ Computer-assistedly discovered by Bill Cutler in 1988 - unique level-7
        one of the 157 unique-level-7 3-hole 6-piece burr"""
    return [
        Piece([2,2], [15,3,10,14,3,15]),
        Piece([2,2], [15,15,1,11,15,15]),
        Piece([2,2], [15,7,2,2,7,15]),
        Piece([2,2], [15,3,2,2,3,15]),
        Piece([2,2], [15,7,2,11,15,15]),
        Piece([2,2], [15,3,3,2,3,15]),
    ]

def CC4H():
    """ Computer-assistedly discovered by Bill Cutler in 1988 - unique level-8
        one of the 13 unique-level-8 4-hole 6-piece burr """
    return [
        Piece([2,2], [15,3,2,2,3,15]),
        Piece([2,2], [15,15,7,6,7,15]),
        Piece([2,2], [15,15,1,1,15,15]),
        Piece([2,2], [15,3,10,10,3,15]),
        Piece([2,2], [15,11,2,2,7,15]),
        Piece([2,2], [15,5,1,11,5,15]),
    ]
