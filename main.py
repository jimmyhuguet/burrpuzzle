import src
import src.puzzles
import src.solve
import src.piece22
import src.piece238
import sys
import json
import random


if __name__ == "__main__":
    args = sys.argv
    if len(args) == 1 or args[1] == "help":
        print("available command:")
        print("    * make: to generate the stl file for a puzzle")
        sys.exit(0)
    if args[1] == "make":
        puzzles = {
            "BBB": src.puzzles.bill_baffling_burr,
            "piston": src.puzzles.piston_puzzle,
            "L5": src.puzzles.L5_notchable,
            "love": src.puzzles.Love_dozen,
            "CC3H": src.puzzles.CC3H,
            "CC4H": src.puzzles.CC4H
        }
        if len(args) < 3 or args[2] == "help" or args[2] not in puzzles:
            print("Available puzzle to make are the following:")
            print(json.dumps(list(puzzles.keys())))
            sys.exit(0)
        if args[2] in puzzles:
            pieces = puzzles[args[2]]()
            for p in pieces:
                p.save(args[2])
            sys.exit(0)
    if args[1] == "explore":
        n_piece238 = src.piece238.get_all_notchable_piece238_for_6pb()
        l = len(n_piece238)
        for i in range(20):
            r = random.randint(0, l-1)
            print(n_piece238[r].__gui__())
            print()
        sys.exit(0)
    if args[1] == "solve":
        src.solve.Puzzle(src.puzzles.Love_dozen()).solve_6pb()
        sys.exit(0)
    if args[1] == "random":
        all_pieces = src.get_all_notchable_piece_for_6pb_66()
        for j in range(1000):
            selected_pieces = []
            for i in range(6):
                r = random.randrange(0,10)
                selected_pieces.append(all_pieces[r])
            print(" -- ".join(map(str, selected_pieces)))
            assemblies, solvable = src.solve.Puzzle(selected_pieces).solve_6pb()
            if len(assemblies) > 0:
                for i, s in solvable.items():
                    print(assemblies[i])
                    print(s)
                print(f"{len(assemblies)} assemblies found, {len(solvable)} can be disassembled")
            print()
    if args[1] == "6pb66":
        pieces = src.get_all_notchable_piece_for_6pb_66()
        r = random.randrange(0,len(pieces))
        pieces[r].save("test")
        sys.exit(0)
    print("Invalid command")
