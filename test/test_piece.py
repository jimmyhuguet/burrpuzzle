import unittest
import src.piece

class TestPiece(unittest.TestCase):

    def test_cubies(self):
        c = src.Piece([2,2], [15,1,2,4,8,7]).cubies
        assert c[0][0][0] == True
        assert c[0][0][1] == True
        assert c[0][1][0] == True
        assert c[0][1][1] == True

        assert c[1][0][0] == True
        assert c[1][0][1] == False
        assert c[1][1][0] == False
        assert c[1][1][1] == False

        assert c[2][0][0] == False
        assert c[2][0][1] == False
        assert c[2][1][0] == True
        assert c[2][1][1] == False

        assert c[3][0][0] == False
        assert c[3][0][1] == True
        assert c[3][1][0] == False
        assert c[3][1][1] == False

        assert c[4][0][0] == False
        assert c[4][0][1] == False
        assert c[4][1][0] == False
        assert c[4][1][1] == True

        assert c[5][0][0] == True
        assert c[5][0][1] == True
        assert c[5][1][0] == True
        assert c[5][1][1] == False

        c = src.Piece([2,3], [1,2,4,8,16,32]).cubies
        assert c[0][0][0] == True
        assert c[0][0][1] == False
        assert c[0][0][2] == False
        assert c[0][1][0] == False
        assert c[0][1][1] == False
        assert c[0][1][2] == False

        assert c[1][0][0] == False
        assert c[1][0][1] == False
        assert c[1][0][2] == False
        assert c[1][1][0] == True
        assert c[1][1][1] == False
        assert c[1][1][2] == False

        assert c[2][0][0] == False
        assert c[2][0][1] == True
        assert c[2][0][2] == False
        assert c[2][1][0] == False
        assert c[2][1][1] == False
        assert c[2][1][2] == False

        assert c[3][0][0] == False
        assert c[3][0][1] == False
        assert c[3][0][2] == False
        assert c[3][1][0] == False
        assert c[3][1][1] == True
        assert c[3][1][2] == False

        assert c[4][0][0] == False
        assert c[4][0][1] == False
        assert c[4][0][2] == True
        assert c[4][1][0] == False
        assert c[4][1][1] == False
        assert c[4][1][2] == False

        assert c[5][0][0] == False
        assert c[5][0][1] == False
        assert c[5][0][2] == False
        assert c[5][1][0] == False
        assert c[5][1][1] == False
        assert c[5][1][2] == True
